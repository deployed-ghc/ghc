FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > ghc.log'

COPY ghc .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' ghc
RUN bash ./docker.sh

RUN rm --force --recursive ghc
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD ghc
